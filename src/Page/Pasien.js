import React, { useEffect } from "react";
// import Sidebar from "../component/Sidebar";
// import gambar from "../image/istockphoto-137506457-612x612-removebg-preview.png";
import { useState } from "react";
import { Modal } from "react-bootstrap";
// import Navbar from "../component/Navbar";
import axios from "axios";
import Swal from "sweetalert2";
import SideBar from "../Component/SideBar";
import Navbar from "../Component/Navbar";

export default function Pasien() {
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const [show, setShow] = useState(false);
  const [show1, setShow1] = useState(false);
  const [namaPasien, setNamaPasien] = useState("");
  const [statusPasien, setStatusPasien] = useState("");
  const [status, setStatus] = useState("belum ditangani");
  const [pasien, setPasien] = useState([]);
  const [periksaPasien, setPeriksaPasien] = useState([]);
  const [pages, setPages] = useState(0);
  const [keluhan, setKeluhan] = useState("");
  const [jabatanGuru, setJabatanGuru] = useState("Guru");
  const [jabatanSiswa, setJabatanSiswa] = useState("Siswa");
  const [jabatanKaryawan, setJabatanKaryawan] = useState("Karyawan");
  const [jabatan, setJabatan] = useState();

  const getAllNamaPasien = async () => {
    await axios
      .get(`http://localhost:2023/daftar-pasien`)
      .then((res) => {
        // setPages(res.data.data.totalPages);
        setPeriksaPasien(res.data.data);
        // setJabatanGuru(res.data.data);
        // setJabatanKaryawan(res.data.data);
        // setJabatanSiswa(res.data.data);
        console.log(res.data.data);
      })
      .catch((error) => {
        alert("Terjadi Kesalahan" + error);
      });
  };

  const [allguru, setAllGuru] = useState([]);
  const getAllGuru = async () => {
    await axios
      .get(`http://localhost:2023/data/getAll-guru`)
      .then((res) => {
        setAllGuru(res.data.data);
        console.log(res.data.data);
      })
      .catch((error) => {
        alert("Terjadi Kesalahan" + error);
      });
  };

  const [allSiswa, setAllSiswa] = useState([]);
  const getAllSiswa = async () => {
    await axios
      .get(`http://localhost:2023/data/all-siswa`)
      .then((res) => {
        setAllSiswa(res.data.data);
        console.log(res.data.data);
      })
      .catch((error) => {
        alert("Terjadi Kesalahan" + error);
      });
  };

  const [allkaryawan, setAllKaryawan] = useState([]);
  const getAllKaryawan = async () => {
    await axios
      .get(`http://localhost:2023/data/all-karyawan`)
      .then((res) => {
        setAllKaryawan(res.data.data);
        console.log(res.data.data);
      })
      .catch((error) => {
        alert("Terjadi Kesalahan" + error);
      });
  };

  const getAllPasien = async () => {
    await axios
      .get(
        `http://localhost:2023/daftar-pasien`
      )
      .then((res) => {
        // setPages(res.data.data.totalPages);
        setPasien(res.data.data);
        console.log(res.data.data);
      })
      .catch((error) => {
        alert("Terjadi Kesalahan" + error);
      });
  };

  const add = async (e) => {
    e.preventDefault();
    e.persist();

    try {
      await axios.post(
        `http://localhost:2023/daftar-pasien?keluhan=${keluhan}&namaPasien=${statusPasien}&status=belum%20ditangani&statusPasien=${namaPasien}`
      );
      setShow(false);
      Swal.fire({
        icon: "success",
        title: "Sukses Menambahkan",
        showConfirmButton: false,
        timer: 1500,
      });
      setTimeout(() => {
        window.location.reload();
      }, 1500);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    getAllNamaPasien(0);
    getAllPasien(0);
    getAllFilter(0);
    getAllGuru(0);
    getAllSiswa(0);
    getAllKaryawan(0);
  }, []);

  const [tanggal, setTanggal] = useState([]);
  const [filter, setFilter] = useState([]);
  const [filterTanggal, setFilterTanggal] = useState([]);
  const [allTanggal, setAllTanggal] = useState({
    tanggal: [],
  });
  const getAllFilter = async () => {
    await axios
      .get(
        `http://localhost:2023/daftar-pasien/filterTanggal?query=${tanggal}`
      )
      .then((res) => {
        setFilterTanggal(res.data.data);
        setTanggal(tanggal);
        console.log(res.data.data);
      })
      .catch((error) => {
        alert("Terjadi Kesalahan" + error);
      });
  };

  return (
    <div className="flex">
      <div>
        <SideBar />
      </div>
      <div className="overflow-x-auto h-[39rem] w-full pl-64">
        <Navbar />
        <div class="overflow-x-auto rounded-lg ml-[2rem] border shadow-lg w-[66rem] shadow-green-100 border-gray-200 h-fit mt-7 bg-gradient-to-t from-rose-100 to-teal-100">
          <div className="flex justify-between text-white font-bold p-3 bg-green-400">
            <p className=" text-xl">Filter Rekap Data</p>
            <button
              class="bg-green-600 hover:bg-green-700 text-white font-bold py-2 px-4 rounded"
              onClick={() => setShow1(true)}
            >
              Filter Tanggal
            </button>
          </div>
          <table class="w-full divide-y-2 divide-gray-200 bg-gray-50 text-sm h-fitt">
            {allTanggal.tanggal ? (
              // <div className="">
              //   <img
              //     src={gambar}
              //     alt=""
              //     className="w-[12rem] ml-[26rem] mt-4"
              //   />
              //   <p className="text-3xl w-[19rem] text-center ml-[22rem] mb-7 mt-7">
              //     Filter Terlebih Dahulu Sesuai Tanggal Yang DiInginkan
              //   </p>
              // </div>
              <div className="mb-5">
                <p className="text-center text-3xl font-semibold">
                  Rekap Data Pasien
                </p>
                <p className="text-center text-xl mt-3">{tanggal}</p>
                <button class="bg-green-500 hover:bg-green-700 ml-[26rem] mt-3 text-white font-bold py-2 px-4 rounded">
                  Download Data Rekap Pasien
                </button>
              </div>
            ) : (
              <></>
            )}
          </table>
        </div>

        <div class="overflow-x-auto w-[66rem] ml-[2rem] mb-5 rounded-lg border shadow-lg shadow-green-100 border-gray-200 h-fit mt-7 bg-gradient-to-t from-rose-100 to-teal-100">
          <div className="flex justify-between text-white font-bold p-3 bg-green-400">
            <p className=" text-xl">Daftar Pasien</p>
            <button
              class="bg-green-600 hover:bg-green-700 text-white font-bold py-2 px-4 rounded"
              onClick={handleShow}
            >
              Tambah
            </button>
          </div>
          <table class="w-full divide-y-2 divide-gray-200 bg-white text-sm">
            <thead class="ltr:text-left rtl:text-right bg-green-200">
              <tr>
                <th class="whitespace-nowrap px- py-2 font-medium text-gray-900">
                  No
                </th>
                <th class="whitespace-nowrap px- py-2 font-medium text-gray-900">
                  Nama Pasien
                </th>
                <th class="whitespace-nowrap px- py-2 font-medium text-gray-900">
                  Status Pasien
                </th>
                <th class="whitespace-nowrap px- py-2 font-medium text-gray-900">
                  Jabatan
                </th>
                <th class="whitespace-nowrap px- py-2 font-medium text-gray-900">
                  Tanggal/Jam
                </th>
                <th class="whitespace-nowrap px- py-2 font-medium text-gray-900">
                  Keterangan
                </th>
                <th class="whitespace-nowrap px- py-2 font-medium text-gray-900">
                  Status
                </th>
                <th class="whitespace-nowrap px- py-2 font-medium text-gray-900">
                  Aksi
                </th>
              </tr>
            </thead>

            <tbody class="divide-y divide-gray-200 bg-gray-100">
              {periksaPasien.map((data, index) => {
                return (
                  <tr key={data.id}>
                    <td class="whitespace-nowrap px-4 py-2 text-center font-medium text-gray-900">
                      {index + 1}
                    </td>
                    <td class="whitespace-nowrap px-4 py-2 text-center text-gray-700">
                      {data.namaPasien.username}
                    </td>
                    <td class="whitespace-nowrap px-4 py-2 text-center text-gray-700">
                      {data.namaPasien.status}
                    </td>
                    <td class="whitespace-nowrap px-4 py-2 text-center text-gray-700">
                      {data.namaPasien.status}
                    </td>
                    <td class="whitespace-nowrap px-4 py-2 text-center text-gray-700">
                      {data.tanggal}
                    </td>
                    <td class="whitespace-nowrap px-4 py-2 text-center text-gray-700">
                      {data.keluhan}
                    </td>
                    {data.status == "sudah ditangani" ? (
                      <>
                        <td class="whitespace-nowrap px-4 py-2 text-center text-green-500">
                          {data.status}
                        </td>{" "}
                      </>
                    ) : (
                      <td class="whitespace-nowrap px-4 py-2 text-center text-red-700">
                        {data.status}
                      </td>
                    )}
                    <td class="whitespace-nowrap px-4 py-2 text-center text-gray-700">
                      {data.status == "sudah ditangani" ? (
                        <>
                        <div className="flex">
                        <i class="fa-solid fa-download "></i>
                          <p class="bg-green-500 text-white font-bold py-2 px-4 rounded ml-10">
                            Sudah Ditangani
                          </p>
                          </div>
                        </>
                      ) : (
                        <a
                          href={"/tanganiPasien/" + data.id}
                          class="bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4 rounded"
                        >
                          Tangani
                        </a>
                      )}
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>

        {/* modal add */}
        <Modal
          show={show}
          onHide={handleClose}
          id="authentication-modal"
          tabIndex="-1"
          aria-hidden="true"
          className="md:ml-[30%] ml-0 fixed top-0 left-0 right-0 z-50 hidden w-full p-4 overflow-x-hidden overflow-y-auto md:inset-0 h-modal md:h-full"
        >
          <div className="relative w-full h-full max-w-md md:h-auto">
            <div className="relative bg-white rounded-lg shadow dark:bg-white">
              <button
                type="button"
                className="absolute top-3 right-2.5 text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 ml-auto inline-flex items-center dark:hover:bg-gray-800 dark:hover:text-white"
                data-modal-hide="authentication-modal"
                onClick={handleClose}
              >
                <svg
                  aria-hidden="true"
                  className="w-5 h-5"
                  fill="currentColor"
                  viewBox="0 0 20 20"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    fillRule="evenodd"
                    d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                    clipRule="evenodd"
                  ></path>
                </svg>
                <span className="sr-only">Close modal</span>
              </button>
              <div className="px-6 py-6 lg:px-8">
                <h3 className="mb-4 text-xl font-medium text-black dark:text-black">
                  Tambah Daftar Pasien
                </h3>
                <form className="space-y-3" onSubmit={add}>
                  <fieldset>
                    <label
                      htmlFor="frm-whatever"
                      className="md:text-base text-sm mt-1 font-bold"
                    >
                      Status Pasien
                    </label>
                    <div className="md:relative border w-[9rem] md:w-[24rem] rounded-lg flex border-gray-300 text-gray-800 bg-white">
                      <select
                        className="appearance-none py-1 px-2 md:text-base w-[24rem]  text-sm h-10 bg-white"
                        name="whatever"
                        id="frm-whatever"
                        onChange={(e) => setNamaPasien(e.target.value)}
                      >
                        <option>Pilih Status Pasien</option>
                        <option value="Guru">Guru</option>
                        <option value="Siswa">Siswa</option>
                        <option value="Karyawan">Karyawan</option>
                      </select>
                      <div className="pointer-events-none md:absolute right-0 top-0 bottom-0 flex items-center px-2 text-gray-700 border-l">
                        <svg
                          className="h-4 w-4"
                          xmlns="http://www.w3.org/2000/svg"
                          viewBox="0 0 20 20"
                        >
                          <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" />
                        </svg>
                      </div>
                    </div>
                  </fieldset>
                  {namaPasien == "Guru" ? (
                    <>
                      <fieldset>
                        <label
                          htmlFor="frm-whatever"
                          className="md:text-base text-sm mt-1 font-bold"
                        >
                          Nama Pasien
                        </label>
                        <div className="md:relative border w-[9rem] md:w-[24rem] rounded-lg flex border-gray-300 text-gray-800 bg-white">
                          <select
                            className="appearance-none py-1 px-2 md:text-base w-[24rem] text-sm h-10 bg-white"
                            name="whatever"
                            id="frm-whatever"
                            onChange={(e) => setStatusPasien(e.target.value)}
                          >
                            <option value="">Pilih Nama Pasien</option>
                            {allguru.map((down, idx) => {
                              return (
                                <option key={idx} value={down.id}>
                                  <p>{down.username}</p>
                                </option>
                              );
                            })}
                          </select>
                          <div className="pointer-events-none md:absolute right-0 top-0 bottom-0 flex items-center px-2 text-gray-700 border-l">
                            <svg
                              className="h-4 w-4"
                              xmlns="http://www.w3.org/2000/svg"
                              viewBox="0 0 20 20"
                            >
                              <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" />
                            </svg>
                          </div>
                        </div>
                      </fieldset>
                    </>
                  ) : namaPasien == "Siswa" ? (
                    <>
                      {" "}
                      <fieldset>
                        <label
                          htmlFor="frm-whatever"
                          className="md:text-base text-sm mt-1 font-bold"
                        >
                          Nama Pasien
                        </label>
                        <div className="md:relative border w-[9rem] md:w-[24rem] rounded-lg flex border-gray-300 text-gray-800 bg-white">
                          <select
                            className="appearance-none py-1 px-2 md:text-base w-[24rem] text-sm h-10 bg-white"
                            name="whatever"
                            id="frm-whatever"
                            onChange={(e) => setStatusPasien(e.target.value)}
                          >
                            <option value="">Pilih Nama Pasien</option>
                            {allSiswa.map((down, idx) => {
                              return (
                                <option key={idx} value={down.id}>
                                  <p>{down.username}</p>
                                </option>
                              );
                            })}
                          </select>
                          <div className="pointer-events-none md:absolute right-0 top-0 bottom-0 flex items-center px-2 text-gray-700 border-l">
                            <svg
                              className="h-4 w-4"
                              xmlns="http://www.w3.org/2000/svg"
                              viewBox="0 0 20 20"
                            >
                              <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" />
                            </svg>
                          </div>
                        </div>
                      </fieldset>{" "}
                    </>
                  ) : namaPasien == "Karyawan" ? (
                    <>
                      {" "}
                      <fieldset>
                        <label
                          htmlFor="frm-whatever"
                          className="md:text-base text-sm mt-1 font-bold"
                        >
                          Nama Pasien
                        </label>
                        <div className="md:relative border w-[9rem] md:w-[24rem] rounded-lg flex border-gray-300 text-gray-800 bg-white">
                          <select
                            className="appearance-none py-1 px-2 md:text-base w-[24rem] text-sm h-10 bg-white"
                            name="whatever"
                            id="frm-whatever"
                            onChange={(e) => setStatusPasien(e.target.value)}
                          >
                            <option value="">Pilih Nama Pasien</option>
                            {allkaryawan.map((down, idx) => {
                              return (
                                <option key={idx} value={down.id}>
                                  <p>{down.username}</p>
                                </option>
                              );
                            })}
                          </select>
                          <div className="pointer-events-none md:absolute right-0 top-0 bottom-0 flex items-center px-2 text-gray-700 border-l">
                            <svg
                              className="h-4 w-4"
                              xmlns="http://www.w3.org/2000/svg"
                              viewBox="0 0 20 20"
                            >
                              <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" />
                            </svg>
                          </div>
                        </div>
                      </fieldset>{" "}
                    </>
                  ) : (
                    <>
                      <fieldset>
                        <label
                          htmlFor="frm-whatever"
                          className="md:text-base text-sm mt-1 font-bold"
                        >
                          Nama Pasien
                        </label>
                        <div className="md:relative border w-[9rem] md:w-[24rem] rounded-lg flex border-gray-300 text-gray-800 bg-white">
                          <select
                            className="appearance-none py-1 px-2 md:text-base w-[24rem] text-sm h-10 bg-white"
                            name="whatever"
                            id="frm-whatever"
                            onChange={(e) => setStatusPasien(e.target.value)}
                          >
                            <option>Pilih Nama Pasien</option>
                          </select>
                          <div className="pointer-events-none md:absolute right-0 top-0 bottom-0 flex items-center px-2 text-gray-700 border-l">
                            <svg
                              className="h-4 w-4"
                              xmlns="http://www.w3.org/2000/svg"
                              viewBox="0 0 20 20"
                            >
                              <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" />
                            </svg>
                          </div>
                        </div>
                      </fieldset>
                    </>
                  )}
                  <div>
                    <label className="block mb-2 text-sm font-bold text-black dark:text-black">
                      Keluhan Pasien
                    </label>
                    <input
                      onChange={(e) => setKeluhan(e.target.value)}
                      value={keluhan}
                      type="text"
                      placeholder=" Keluhan Pasien"
                      className="bg-white border border-gray-300 text-black text-sm rounded-lg focus:ring-green-500 focus:border-green-500 block w-full md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
                      required
                    />
                  </div>
                  <button
                    type="submit"
                    className="w-full text-white bg-green-700 hover:bg-green-800 focus:ring-4 focus:outline-none focus:ring-green-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-green-600 dark:hover:bg-green-700 dark:focus:ring-green-800"
                  >
                    Tambah
                  </button>
                </form>
              </div>
            </div>
          </div>
        </Modal>

        <Modal
          show={show1}
          onHide={!show1}
          id="authentication-modal"
          tabIndex="-1"
          aria-hidden="true"
          className="md:ml-[30%] ml-0 fixed top-0 left-0 right-0 z-50 hidden w-full p-4 overflow-x-hidden overflow-y-auto md:inset-0 h-modal md:h-full"
        >
          <div className="relative w-full h-full max-w-md md:h-auto">
            <div className="relative bg-white rounded-lg shadow dark:bg-white">
              <button
                type="button"
                className="absolute top-3 right-2.5 text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 ml-auto inline-flex items-center dark:hover:bg-gray-800 dark:hover:text-white"
                data-modal-hide="authentication-modal"
                onClick={() => setShow1(false)}
              >
                <svg
                  aria-hidden="true"
                  className="w-5 h-5"
                  fill="currentColor"
                  viewBox="0 0 20 20"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    fillRule="evenodd"
                    d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                    clipRule="evenodd"
                  ></path>
                </svg>
                <span className="sr-only">Close modal</span>
              </button>
              <div className="px-6 py-6 lg:px-8">
                <h3 className="mb-4 text-xl font-medium text-black dark:text-black">
                  Filter Rekap Data Pasien
                </h3>
                <form className="space-y-3">
                  {/* <div>
                    <label className="block mb-2 text-sm font-bold text-black dark:text-black">
                      Sampai Tanggal :
                    </label>
                    <input
                      type="date"
                      className="bg-white border border-gray-300 text-black text-sm rounded-lg focus:ring-green-500 focus:border-green-500 block w-full md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
                      required
                      value={tanggal}
                      onChange={(e) => setTanggal(e.target.value)}
                    />
                  </div> */}

                  <div>
                    <label className="block mb-2 text-sm font-bold text-black dark:text-black">
                      Tanggal :
                    </label>
                    <input
                      type="date"
                      className="bg-white border border-gray-300 text-black text-sm rounded-lg focus:ring-green-500 focus:border-green-500 block w-full md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
                      required
                      value={tanggal}
                      onChange={(e) => setTanggal(e.target.value)}
                    />
                  </div>
                  <button
                    type="submit"
                    onClick={(e) => {
                      e.preventDefault();
                      setShow1(false);
                      getAllFilter(0);
                    }}
                    className="w-full text-white bg-green-700 hover:bg-green-800 focus:ring-4 focus:outline-none focus:ring-green-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-green-600 dark:hover:bg-green-700 dark:focus:ring-green-800"
                  >
                    Filter
                  </button>
                </form>
              </div>
            </div>
          </div>
        </Modal>
      </div>
    </div>
  );
}
