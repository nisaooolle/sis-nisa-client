import React, { useEffect, useState } from "react";
import SideBar from "../Component/SideBar";
import axios from "axios";
import { Modal } from "react-bootstrap";
import Swal from "sweetalert2";
import Navbar from "../Component/Navbar";

export default function DaftarSiswa() {
  const [siswa, setSiswa] = useState([]);
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const [username, setNamaSiswa] = useState("");
  const [tanggalLahir, setTanggalLahir] = useState("");
  const [tempat, settempatLahir] = useState("");
  const [alamat, setAlamat] = useState("");
  const [kelas, setKelas] = useState("");
  const [excel, setExcel] = useState("");

  //   const history = useHistory();
  const allSiswa = async () => {
    await axios
      .get(`http://localhost:2023/data/all-siswa`)
      .then((res) => {
        setSiswa(res.data.data);
      })
      .catch((err) => {
        alert("Terjadi Kesalahan Sir " + err);
      });
  };
  const postPena = async (e) => {
    e.preventDefault();

    try {
      const { status } = await axios.post(`http://localhost:2023/data/post-siswa?alamat=${alamat}&kelas=${kelas}&tanggalLahir=${tanggalLahir}&tempat=${tempat}&username=${username}`, {
      });
      // Jika respon 200/ ok
      if (status === 200) {
        Swal.fire({
          icon: "success",
          title: "Menambahkan data sukses!!",
          showConfirmButton: false,
          timer: 1500,
        });
        setTimeout(() => {
          window.location.reload();
        }, 1500);
      }
    } catch (error) {
      Swal.fire({
        icon: "error",
        // title: "Username atau password tidak valid!",
        showConfirmButton: false,
        timer: 1500,
      });
      console.log(error);
    }
  };

  const deleteRak = async (id) => {
    Swal.fire({
      title: "Apakah Anda Ingin Menghapus?",
      text: "Perubahan data tidak bisa dikembalikan!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Hapus",
      cancelButtonText: "Batal",
    }).then((result) => {
      if (result.isConfirmed) {
        axios.delete("http://localhost:2023/data/" + id);
        Swal.fire({
          icon: "success",
          title: "Dihapus!",
          showConfirmButton: false,
          timer: 1500,
        });
        setTimeout(() => {
          window.location.reload();
        }, 1500);
      }
    });
  };

  const download = async () => {
    await Swal.fire({
      title: "Apakah Anda Yakin?",
      text: "Data akan didownload!",
      icon: "question",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Ya, download!",
    }).then((result) => {
      if (result.isConfirmed) {
        axios({
          url: "http://localhost:2023/siswa/excel",
          method: "GET",
          responseType: "blob",
        }).then((response) => {
          var fileURL = window.URL.createObjectURL(new Blob([response.data]));
          var fileLink = document.createElement("a");

          fileLink.href = fileURL;
          fileLink.setAttribute("download", "data-siswa.xlsx");
          document.body.appendChild(fileLink);

          fileLink.click();
        });
        Swal.fire("Download!", "File Anda Telah Di Unduh", "success");
      }
    });
  };

  const handleShowExcel = () => setShowExcel(true);
  const [showExcel, setShowExcel] = useState(false);
  const handleCloseExcel = () => setShowExcel(false);
  const [loading, setLoading] = useState(false);

  const downloadFormat = async (e) => {
    e.preventDefault();
    await Swal.fire({
      title: "Yakin ingin mendownload?",
      text: "Ini adalah file format excel untuk mengimport data.",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#0b409c",
      cancelButtonColor: "#d33",
      confirmButtonText: "Ya, download!",
      cancelButtonText: "Batal",
      timer: 1500
    }).then((result) => {
      if (result.isConfirmed) {
        axios({
          url: `http://localhost:2023/siswa/excel/download/template
          `,
          method: "GET",
          responseType: "blob",
        }).then((response) => {
          setLoading(true);
          setTimeout(() => {
            var fileURL = window.URL.createObjectURL(new Blob([response.data]));
            var fileLink = document.createElement("a");

            fileLink.href = fileURL;
            fileLink.setAttribute("download", "format-data-siswa.xlsx");
            document.body.appendChild(fileLink);

            fileLink.click();
            handleCloseExcel();
            setLoading(false);
          }, 2000);
        });
      }
    });
  };

  const importExcel = async (e) => {
    e.preventDefault();

    const formData = new FormData();

    formData.append("file", excel);
    formData.append("username", username);
    formData.append("alamat", alamat);
    formData.append("tanggalLahir", tanggalLahir);
    formData.append("tempat", tempat);
    formData.append("kelas", kelas);

    await axios
      .post(`http://localhost:2023/siswa/excel/upload/siswa`, formData)
      .then(() => {
        Swal.fire("Sukses!", " berhasil ditambahkan.", "success");
        window.location.reload();
      })
      .catch((err) => {
        console.log(err);
        Swal.fire("Error", "Anda belum memilih file untuk diimport!.", "error");
      });
  };

  useEffect(() => {
    allSiswa();
  }, []);
  return (
    <div>
      <Navbar />
      <SideBar />
      <div class="overflow-x-auto ml-80 mr-28 mt-10 shadow-lg shadow-green-900">

        <h1 className="flex gap-3 bg-green-900 p-3 rounded-t">
          <p className="mt-1 text-white text-xl ">Daftar Siswa</p>
          <button
            className="btn btn-primary ml-auto bg-green-900 w-[15rem] h-8 inline-block rounded border border-current px-4 py-2 text-sm font-medium text-white transition hover:scale-110 hover:shadow-xl focus:outline-none focus:ring active:text-green-500"
            type="submit"
            onClick={handleShow}
          >
            Tambah
          </button>
          <p>
            <button
              type="button"
              className="btn btn-primary bg-green-900 w-[15rem] h-8 inline-block rounded border border-current px-4 py-2 text-sm font-medium text-white transition hover:scale-110 hover:shadow-xl focus:outline-none focus:ring active:text-green-500"
              onClick={handleShowExcel}
            >
              Import Data
            </button>
            <p className="mt-2">
              <button
                type="submit"
                onClick={download}
                className="btn btn-primary bg-green-900 w-[15rem] h-8 inline-block rounded border border-current px-4 py-2 text-sm font-medium text-white transition hover:scale-110 hover:shadow-xl focus:outline-none focus:ring active:text-green-500"
              >
                Download Data
              </button>
            </p>
          </p>
        </h1>
        <table class="min-w-full divide-y-2 divide-gray-200 bg-white text-sm">
          <thead class="ltr:text-left rtl:text-right">
            <tr className="text-center">
              <th class="whitespace-nowrap px-10 py-2 font-medium text-gray-900">
                No
              </th>
              <th class="whitespace-nowrap px-10 py-2 font-medium text-gray-900">
                Nama Siswa
              </th>
              <th class="whitespace-nowrap px-10 py-2 font-medium text-gray-900">
                Kelas
              </th>
              <th class="whitespace-nowrap px-10 py-2 font-medium text-gray-900">
                Tempat Tanggal Lahir
              </th>
              <th class="whitespace-nowrap px-10 py-2 font-medium text-gray-900">
                Alamat
              </th>
              <th class="whitespace-nowrap px-10 py-2 font-medium text-gray-900">
                Aksi
              </th>
            </tr>
          </thead>

          <tbody class="divide-y divide-gray-200 ">
            {siswa.map((data, index) => (
              <tr class="odd:bg-gray-50 text-center" key={siswa.id}>
                <td class="whitespace-nowrap px-10 py-2 font-medium text-gray-900">
                  {index + 1}
                </td>
                <td class="whitespace-nowrap px-10 py-2 text-gray-700">
                  {data.username}
                </td>
                <td class="whitespace-nowrap px-10 py-2 text-gray-700">
                  {data.kelas}
                </td>
                <td class="whitespace-nowrap px-10 py-2 text-gray-700">
                  {data.tempat} {data.tanggalLahir}
                </td>
                <td class="whitespace-nowrap px-10 py-2 text-gray-700">
                  {data.alamat}
                </td>
                <td>
                  <a
                    href={"/editSiswa/" + data.id}
                    style={{ marginRight: "5px" }}
                  >
                    <button className="bg-blue-500 hover:bg-blue-700 text-white md:text-sm text-xs font-bold py-1 px-2 rounded">
                      <i className="fas fa-edit"></i>
                    </button>
                  </a>
                  <button
                    className="bg-red-500 hover:bg-red-700 text-white font-bold py-1 md:text-sm text-xs px-2 rounded"
                    onClick={() => deleteRak(data.id)}
                  >
                    <i className="fas fa-trash-alt"></i>
                  </button>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
      {/* modal add */}
      <Modal
        show={show}
        onHide={handleClose}
        id="authentication-modal"
        tabIndex="-1"
        aria-hidden="true"
        className="md:ml-[30%] ml-2 fixed top-0 left-0 right-0 z-50 hidden w-full p-4 overflow-x-hidden overflow-y-auto md:inset-0 h-modal md:h-full"
      >
        <div className="relative w-full h-full max-w-md md:h-auto">
          <div className="relative bg-white rounded-lg shadow border-2 dark:bg-white text-black ">
            <button
              type="button"
              className="absolute top-3 right-2.5 text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 ml-auto inline-flex items-center dark:hover:bg-gray-800 dark:hover:text-white"
              data-modal-hide="authentication-modal"
              onClick={handleClose}
            >
              <svg
                aria-hidden="true"
                className="w-5 h-5"
                fill="currentColor"
                viewBox="0 0 20 20"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  fillRule="evenodd"
                  d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                  clipRule="evenodd"
                ></path>
              </svg>
              <span className="sr-only">Close modal</span>
            </button>
            <div className="px-6 py-6 lg:px-8">
              <h3 className="mb-4 md:text-xl text-base font-medium text-black dark:text-black">
                Tambahkan Daftar Siswa
              </h3>
              <form className="space-y-3" onSubmit={postPena}>
                <div>
                  <label className="block mb-2 text-sm font-medium text-black dark:text-black">
                    Nama Siswa
                  </label>
                  <input
                    placeholder="Nama Siswa"
                    onChange={(e) => setNamaSiswa(e.target.value)}
                    value={username}
                    className="bg-gray-50 border border-gray-300 text-black text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-500 dark:text-black"
                    required
                  />
                </div>
                <div>
                  <label className="block mb-2 text-sm font-medium text-black dark:text-black">
                    Kelas
                  </label>
                  <select
                    onChange={(e) => setKelas(e.target.value)}
                    className="bg-gray-50 border border-gray-300 text-black text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-500 dark:text-black"
                    aria-label="kelas"
                  >
                    <option class="hidden">Pilih Kelas</option>
                    <option value="VII A">VII A</option>
                    <option value="VII B"> VII B</option>
                    <option value="VII C">VII C</option>
                    <option value="VII D">VII D</option>
                    <option value="VII E">VII E</option>
                    <option value="VII F">VII F</option>
                    <option value="VII G">VII G</option>
                    <option value="VII H">VII H</option>
                    <option value="VII I">VII I</option>
                    <option value="VIII A">VIII A</option>
                    <option value="VIII B">VIII B</option>
                    <option value="VIII C">VIII C</option>
                    <option value="VIII D">VIII D</option>
                    <option value="VIII E">VIII E</option>
                    <option value="VIII F">VIII F</option>
                    <option value="VIII G">VIII G</option>
                    <option value="VIII H">VIII H</option>
                    <option value="VIII I">VIII I</option>
                    <option value="IX A">IX A</option>
                    <option value="IX B">IX B</option>
                    <option value="IX C">IX C</option>
                    <option value="IX D">IX D</option>
                    <option value="IX E">IX E</option>
                    <option value="IX F">IX F</option>
                    <option value="IX G">IX G</option>
                    <option value="IX H">IX H</option>
                    <option value="IX I">IX I</option>
                  </select>
                </div>
                <div>
                  <label className="block mb-2 text-sm font-medium text-black dark:text-black">
                    Tempat Lahir
                  </label>
                  <input
                    placeholder="Tempat Lahir"
                    onChange={(e) => settempatLahir(e.target.value)}
                    value={tempat}
                    className="bg-gray-50 border border-gray-300 text-black text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-500 dark:text-black"
                    required
                  />
                </div>
                <div>
                  <label className="block mb-2 text-sm font-medium text-black dark:text-black">
                    Tanggal Lahir
                  </label>
                  <input
                    placeholder="Tanggal Lahir"
                    onChange={(e) => setTanggalLahir(e.target.value)}
                    value={tanggalLahir}
                    type="date"
                    className="bg-gray-50 border border-gray-300 text-black text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-500 dark:text-black"
                    required
                  />
                </div>
                <div>
                  <label className="block mb-2 text-sm font-medium text-black dark:text-black">
                    Alamat
                  </label>
                  <input
                    placeholder="Alamat"
                    onChange={(e) => setAlamat(e.target.value)}
                    value={alamat}
                    className="bg-gray-50 border border-gray-300 text-black text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-500 dark:text-black"
                    required
                  />
                </div>
                {/* <button
                  onClick={handleClose}
                  type="submit"
                  className="w-40 text-white bg-red-700 hover:bg-red-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                >
                  Batal
                </button>{" "}
                || */}
                <button
                  onClick={handleClose}
                  type="submit"
                  className="w-40 text-white bg-green-900 hover:bg-green-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                >
                  Simpan
                </button>
              </form>
            </div>
          </div>
        </div>
      </Modal>

      {/* Modal Upload Excel */}
      <Modal
        show={showExcel}
        onHide={handleCloseExcel}
        id="authentication-modal"
        tabindex="-1"
        aria-hidden="true"
        className="md:ml-[30%] ml-0 fixed top-0 left-0 right-0 z-50 hidden md:w-full p-4 overflow-x-hidden overflow-y-auto md:inset-0 h-modal md:h-full outline-none ring-0"
      >
        <div className="relative w-full h-full max-w-md md:h-auto">
          <div className="relative bg-white rounded-lg shadow dark:bg-gray-700">
            <button
              type="button"
              className="absolute top-3 right-2.5 text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 ml-auto inline-flex items-center dark:hover:bg-gray-800 dark:hover:text-white"
              data-modal-hide="authentication-modal"
              onClick={handleCloseExcel}
            >
              <svg
                aria-hidden="true"
                className="w-5 h-5"
                fill="currentColor"
                viewBox="0 0 20 20"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  fillRule="evenodd"
                  d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                  clipRule="evenodd"
                ></path>
              </svg>
              <span className="sr-only">Close modal</span>
            </button>
            <form >
              <div className="px-6 py-6 lg:px-8">
                <h3 className="mb-2 text-2xl font-medium text-gray-900 dark:text-white">
                Import Siswa Dari File Excel
                </h3>
                <hr  className="mb-4"/>
                <div className="border rounded-xl">
                <p className="mb-2">
                  download file dibawah untuk menginput data siswa anda.
                </p>
                <p className="font-bold text-sm">(*column tanggal lahir diubah menjadi short date )</p>
                </div>
                <br />
                <button
                  className="bg-green-700
              text-white hover:bg-green-600 mb-4
              focus:ring-4 focus:outline-none focus:ring-green-300 
              font-medium rounded-lg text-md w-full sm:w-auto px-4 py-2 text-center"
                  onClick={downloadFormat}
                >
                  Download Template Excel
                </button>
                <div>
                  <label className="block mb-2 mt-4 font-bold text-sm  text-gray-900 dark:text-white">
                    file
                  </label>
                  <input
                    autoComplete="off"
                    type="file"
                    accept=".xlsx"
                    onChange={(e) => setExcel(e.target.files[0])}
                  />
                </div>
                <button
                  className="bg-green-500
              text-white hover:bg-green-600 
              focus:ring-4 focus:outline-none focus:ring-green-300 
              font-medium rounded-lg text-md w-full sm:w-auto px-4 py-2 text-center mt-4"
                  type="submit"
                  onClick={importExcel}
                >
                  Simpan
                </button>
              </div>
            </form>
          </div>
        </div>
      </Modal>
    </div>
  );
}
