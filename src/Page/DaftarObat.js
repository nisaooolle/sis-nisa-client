import React, { useEffect, useState } from "react";
import SideBar from "../Component/SideBar";
import axios from "axios";
import { Modal } from "react-bootstrap";
import Swal from "sweetalert2";
import Navbar from "../Component/Navbar";

export default function DaftarObat() {
  const [nama, setObat] = useState([]);
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const [namaObat, setNamaObat] = useState("");
  const [stock, setStock] = useState("");
  const [tglexp, setTglexp] = useState("");
  //   const history = useHistory();
  const allobat = async () => {
    await axios
      .get(`http://localhost:2023/daftar-obat/all-obat`)
      .then((res) => {
        setObat(res.data.data);
      })
      .catch((err) => {
        alert("Terjadi Kesalahan Sir " + err);
      });
  };

  const postPena = async (e) => {
    e.preventDefault();

    try {
      const { status } = await axios.post("http://localhost:2023/daftar-obat", {
        namaObat: namaObat,
        stock: stock
      });
      // Jika respon 200/ ok
      if (status === 200) {
        Swal.fire({
          icon: "success",
          title: "Menambahkan data sukses!!",
          showConfirmButton: false,
          timer: 1500,
        });
        setTimeout(() => {
          window.location.reload();
        }, 1500);
      }
    } catch (error) {
      Swal.fire({
        icon: "error",
        title: "Username atau password tidak valid!",
        showConfirmButton: false,
        timer: 1500,
      });
      console.log(error);
    }
  };

  const deleteRak = async (id) => {
    Swal.fire({
      title: "Apakah Anda Ingin Menghapus?",
      text: "Perubahan data tidak bisa dikembalikan!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Hapus",
      cancelButtonText: "Batal",
    }).then((result) => {
      if (result.isConfirmed) {
        axios.delete("http://localhost:2023/daftar-obat/" + id);
        Swal.fire({
          icon: "success",
          title: "Dihapus!",
          showConfirmButton: false,
          timer: 1500,
        });
        setTimeout(() => {
          window.location.reload();
        }, 1500);
      }
    });
  };

  useEffect(() => {
    allobat();
  }, []);
  return (
    <div>
      <Navbar />
      <SideBar />
      <div class="overflow-x-auto ml-80 mr-28 mt-10 shadow-lg shadow-green-900">
        <h2 className="text-center border h-14 text-xl bg-green-900 text-white rounded-lg flex justify-between">
          <p className="mt-3 ml-5">Daftar Obat</p>{" "}
          <p className="mr-5 mt-2">
            <a
              class="inline-block rounded border border-current px-4 py-2 text-sm font-medium text-white transition hover:scale-110 hover:shadow-xl focus:outline-none focus:ring active:text-green-500"
              type="submit"
              onClick={handleShow}
            >
              Tambahkan
            </a>
          </p>
        </h2>
        <table class="min-w-full divide-y-2 divide-gray-200 bg-white text-sm">
          <thead class="ltr:text-left rtl:text-right">
            <tr className="text-center">
              <th class="whitespace-nowrap px-10 py-2 font-medium text-gray-900">
                No
              </th>
              <th class="whitespace-nowrap px-10 py-2 font-medium text-gray-900">
                Nama Obat
              </th>
              <th class="whitespace-nowrap px-10 py-2 font-medium text-gray-900">
                Stock
              </th>
              <th class="whitespace-nowrap px-10 py-2 font-medium text-gray-900">
                Aksi
              </th>
            </tr>
          </thead>

          <tbody class="divide-y divide-gray-200">
            {nama.map((data, index) => (
              <tr class="odd:bg-gray-50 text-center" key={nama.id}>
                <td class="whitespace-nowrap px-10 py-2 font-medium text-gray-900">
                  {index + 1}
                </td>
                <td class="whitespace-nowrap px-10 py-2 text-gray-700">
                  {data.namaObat}
                </td>
                <td class="whitespace-nowrap px-10 py-2 text-gray-700">
                  {data.stock}
                </td>
                <td>
                  <a
                    href={"/editObat/" + data.id}
                    style={{ marginRight: "5px" }}
                  >
                    <button className="bg-blue-500 hover:bg-blue-700 text-white md:text-sm text-xs font-bold py-1 px-2 rounded">
                      <i className="fas fa-edit"></i>
                    </button>
                  </a>
                  <button
                    className="bg-red-500 hover:bg-red-700 text-white font-bold py-1 md:text-sm text-xs px-2 rounded"
                    onClick={() => deleteRak(data.id)}
                  >
                    <i className="fas fa-trash-alt"></i>
                  </button>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>

      {/* modal add */}
      <Modal
        show={show}
        onHide={handleClose}
        id="authentication-modal"
        tabIndex="-1"
        aria-hidden="true"
        className="md:ml-[30%] ml-2 fixed top-0 left-0 right-0 z-50 hidden w-full p-4 overflow-x-hidden overflow-y-auto md:inset-0 h-modal md:h-full"
      >
        <div className="relative w-full h-full max-w-md md:h-auto">
          <div className="relative bg-white rounded-lg shadow border-2 dark:bg-white text-black ">
            <button
              type="button"
              className="absolute top-3 right-2.5 text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 ml-auto inline-flex items-center dark:hover:bg-gray-800 dark:hover:text-white"
              data-modal-hide="authentication-modal"
              onClick={handleClose}
            >
              <svg
                aria-hidden="true"
                className="w-5 h-5"
                fill="currentColor"
                viewBox="0 0 20 20"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  fillRule="evenodd"
                  d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                  clipRule="evenodd"
                ></path>
              </svg>
              <span className="sr-only">Close modal</span>
            </button>
            <div className="px-6 py-6 lg:px-8">
              <h3 className="mb-4 md:text-xl text-base font-medium text-black dark:text-black">
                Tambahkan Daftar Obat
              </h3>
              <form className="space-y-3" onSubmit={postPena}>
                <div>
                  <label className="block mb-2 text-sm font-medium text-black dark:text-black">
                    Nama Obat
                  </label>
                  <input
                    placeholder="Nama Penyakit"
                    onChange={(e) => setNamaObat(e.target.value)}
                    value={namaObat}
                    className="bg-gray-50 border border-gray-300 text-black text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-500 dark:text-black"
                    required
                  />
                </div>
                <div>
                  <label className="block mb-2 text-sm font-medium text-black dark:text-black">
                    Stock
                  </label>
                  <input
                    placeholder="stock"
                    onChange={(e) => setStock(e.target.value)}
                    value={stock}
                    type="number"
                    className="bg-gray-50 border border-gray-300 text-black text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-500 dark:text-black"
                    required
                  />
                </div>
                <div>
                  <label className="block mb-2 text-sm font-medium text-black dark:text-black">
                    Tanggal Expired
                  </label>
                  <input
                    placeholder="Tanggal Expired"
                    onChange={(e) => setTglexp(e.target.value)}
                    value={tglexp}
                    type="date"
                    className="bg-gray-50 border border-gray-300 text-black text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-500 dark:text-black"
                    required
                  />
                </div>
                {/* <button
                  onClick={handleClose}
                  type="submit"
                  className="w-40 text-white bg-red-700 hover:bg-red-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                >
                  Batal
                </button>{" "}
                || */}
                <button
                  onClick={handleClose}
                  type="submit"
                  className="w-40 text-white bg-green-900 hover:bg-green-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                >
                  Simpan
                </button>
              </form>
            </div>
          </div>
        </div>
      </Modal>
    </div>
  );
}
