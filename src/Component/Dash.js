import React, { useEffect, useState } from "react";
import SideBar from "./SideBar";
import axios from "axios";
import Navbar from "./Navbar";

export default function Dash() {
  const [karyawan, setKaryawan] = useState([]);
  const [siswa, setSiswa] = useState([]);
  const [guru, setGuru] = useState([]);
    const [periksaPasien, setPeriksaPasien] = useState([]);


  const allKaryawan = async () => {
    await axios
      .get(`http://localhost:2023/data/all-karyawan`)
      .then((res) => {
        setKaryawan(res.data.data);
      })
      .catch((err) => {
        alert("Terjadi Kesalahan Sir " + err);
      });
  };

  const allSiswa = async () => {
    await axios
      .get(`http://localhost:2023/data/all-siswa`)
      .then((res) => {
        setSiswa(res.data.data);
      })
      .catch((err) => {
        alert("Terjadi Kesalahan Sir " + err);
      });
  };

  const allGuru = async () => {
    await axios
      .get(`http://localhost:2023/data/getAll-guru`)
      .then((res) => {
        setGuru(res.data.data);
      })
      .catch((err) => {
        alert("Terjadi Kesalahan Sir " + err);
      });
  };

  const getAllNamaPasien = async () => {
    await axios
      .get(`http://localhost:2023/daftar-pasien`)
      .then((res) => {
        // setPages(res.data.data.totalPages);
        setPeriksaPasien(res.data.data);
        console.log(res.data.data);
      })
      .catch((error) => {
        alert("Terjadi Kesalahan" + error);
      });
  };

  useEffect(() => {
    allKaryawan();
    allSiswa();
    allGuru();
    getAllNamaPasien(0);
  }, []);
  return (
    <div>
      <Navbar/>
      <SideBar />
      <div className="flex flex-warp gap-16 ml-80 h-24 w-9/12 mt-12 font-serif ">
        <a
          href="/guru"
          class="hover:scale-105 shadow-lg shadow-green-900 block max-w-sm p-6 bg-white border border-gray-200 rounded-lg  hover:bg-gray-100 dark:bg-gray-800 dark:border-gray-700 dark:hover:bg-gray-700 "
        >
          <h5 class="text-xl font-semibold ">Daftar Pasien Guru</h5>
          <p class="text-gray-700 dark:text-gray-400 text-3xl ">
            <i class="fa-solid fa-wheelchair"> {guru.length}</i> Guru
          </p>
        </a>

        <a
          href="/siswa"
          class="hover:scale-105 shadow-lg shadow-green-900 block max-w-sm p-6 bg-white border border-gray-200 rounded-lg  hover:bg-gray-100 dark:bg-gray-800 dark:border-gray-700 dark:hover:bg-gray-700"
        >
          <h5 class="text-xl font-semibold">Daftar Pasien Siswa</h5>
          <p class="font-normal text-gray-700 dark:text-gray-400 text-3xl ">
            <i class="fa-solid fa-wheelchair"> {siswa.length}</i> Siswa
          </p>
        </a>

        <a
          href="/karyawan"
          class="hover:scale-105 shadow-lg shadow-green-900 block max-w-sm p-6 bg-white border border-gray-200 rounded-lg  hover:bg-gray-100 dark:bg-gray-800 dark:border-gray-700 dark:hover:bg-gray-700"
        >
          <h5 class="text-xl font-semibold">Daftar Pasien Karyawan</h5>
          <p class="font-normal text-gray-700 dark:text-gray-400 text-3xl ">
            <i class="fa-solid fa-wheelchair"> {karyawan.length}</i> Karyawan
          </p>
        </a>
      </div>
      <br /><br />
      <div class="overflow-x-auto ml-80 mr-28 shadow-lg shadow-green-900">
        <h2 className="text-center border h-14 text-xl bg-green-900 text-white rounded-lg "><p className="mt-3">
          Riwayat Pasien</p>
        </h2>
        <table class="min-w-full divide-y-2 divide-gray-200 bg-white text-sm">
          <thead class="ltr:text-left rtl:text-right">
          <tr>
              <th class="whitespace-nowrap px-16 py-2 font-medium text-gray-900">
                No
              </th>
              <th class="whitespace-nowrap px-16 py-2 font-medium text-gray-900">
                Nama Pasien
              </th>
              <th class="whitespace-nowrap px-16 py-2 font-medium text-gray-900">
                Status Pasien
              </th>
              <th class="whitespace-nowrap px-16 py-2 font-medium text-gray-900">
                Tanggal/Jam Periksa
              </th>
            </tr>
          </thead>

          <tbody class="divide-y divide-gray-200 bg-gray-100">
              {periksaPasien.map((data, index) => {
                return (
                  <tr key={data.id}>
                    <td class="whitespace-nowrap px-4 py-2 text-center font-medium text-gray-900">
                      {index + 1}
                    </td>
                    <td class="whitespace-nowrap px-4 py-2 text-center text-gray-700">
                      {data.namaPasien.username}
                    </td>
                    <td class="whitespace-nowrap px-4 py-2 text-center text-gray-700">
                      {data.namaPasien.status}
                    </td>
                    <td class="whitespace-nowrap px-4 py-2 text-center text-gray-700">
                      {data.tanggal}
                    </td>
                  </tr>
                );
              })}
            </tbody>
        </table>
      </div>
    </div>
  );
}
