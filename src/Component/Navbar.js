import axios from "axios";
import { useEffect } from "react";
import { useState } from "react";


export default function Navbar() {
  const [profile, setProfile] = useState([]);

  const getAll = async () => {
    await axios
      .get("http://localhost:2023/register/" + localStorage.getItem("Id"))
      .then((res) => {
        setProfile(res.data.data);
      })
      .catch((error) => {
        alert("Terjadi Kesalahan" + error);
      });
  };

  useEffect(() => {
    getAll();
  }, []);
  return (
    <div>
      <div className="bg-green-900 md:w-full h-[3rem] text-white flex">
        <div className="ml-auto">
          {profile.foto ? (
            <a href="/proff">
              <img
                className="w-8 h-8 rounded-full"
                src={profile.foto}
                alt=""
              />
            </a>
          ) : (
            <a href="/proff">
              <img
                className=" mt-2 rounded-full w-16 border-black border"
                src="https://static.vecteezy.com/system/resources/previews/008/442/086/original/illustration-of-human-icon-user-symbol-icon-modern-design-on-blank-background-free-vector.jpg"
                alt=""
              />
            </a>
          )}
        </div>
      </div>
    </div>
  );
}
