import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import Swal from "sweetalert2";
import moment from "moment/moment";
import axios from "axios";

export default function SideBar() {
  let time = new Date().toLocaleTimeString();
  let tanggal = moment().locale("id");
  const [currentTime, setCurrentTime] = useState(time);

  const updateTime = () => {
    let time = new Date().toLocaleTimeString();
    setCurrentTime(time);
  };
  setInterval(updateTime, 1000);
  const [open, setOpen] = useState(false);
  const history = useHistory();

  const logout = () => {
    Swal.fire({
      title: "Anda Yakin Ingin Logout",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Ya",
      cancelButtonText: "Batal",
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire({
          icon: "success",
          title: "Berhasil Logout",
          showConfirmButton: false,
          timer: 1500,
        });
        //Untuk munuju page selanjutnya
        history.push("/");
        setTimeout(() => {
          window.location.reload();
        }, 1500);
        localStorage.clear();
      }
    });
  };
 
  return (
    <div>
      <aside
        id="sidebar-multi-level-sidebar"
        class="fixed top-0 left-0 z-40 w-64 h-screen transition-transform -translate-x-full sm:translate-x-0 bg-green-900 text-white"
        aria-label="Sidebar"
      >
        <div class="h-full px-3 py-4 w-full ">
          <div className="font-semibold text-lg ml-5">
            SISTEM APLIKASI UKS SMPN 1 SEMARANG
          </div>
          <hr className="h-1 bg-green-700" />{" "}
          <ul class="space-y-2 font-medium">
            <li>
              <a
                href="/dash"
                class="flex items-center p-2 text-white rounded-lg  hover:bg-green-500 text-lg mt-1"
              >
                <i class="fa-solid fa-gauge"></i>
                <span class="ml-3">Dashboard</span>
              </a>
            </li>
            <details className="group [&_summary::-webkit-details-marker]:hidden ">
              <summary className="flex cursor-pointer items-center justify-between  rounded-lg py-2 text-white hover:bg-green-500 md:p-2 ">
                <div className="flex items-center gap-2 overflow-hidden">
                  <i class="fa-sharp fa-solid fa-server"></i>{" "}
                  <span className={`text-lg font-medium ${open && "hidden"}`}>
                    Data
                  </span>
                </div>

                <span
                  className={`shrink-0 transition duration-300 group-open:-rotate-90 ${
                    open && "hidden"
                  }`}
                >
                  <i className="fa-solid fa-caret-left"></i>{" "}
                </span>
              </summary>

              <a
                href="/guru"
                className={`flex items-center gap-2 rounded-lg  text-white hover:bg-green-500 md:p-2  
                   `}
              >
                <i class="fa-sharp fa-solid fa-graduation-cap ml-7"></i>
                <span className={`text-lg font-medium ${open && "hidden"}`}>
                  Daftar Guru
                </span>
              </a>
              <a
                href="/siswa"
                className={`flex items-center gap-2 rounded-lg  text-white hover:bg-green-500 md:p-2  
                   `}
              >
                <i class="fa-solid fa-user-group ml-7"></i>{" "}
                <span className={`text-lg font-medium ${open && "hidden"}`}>
                  Daftar Siswa
                </span>
              </a>
              <a
                href="/karyawan"
                className={`flex items-center gap-2 rounded-lg  text-white hover:bg-green-500 md:p-2  
                   `}
              >
                <i class="fa-solid fa-users-line ml-7"></i>
                <span className={`text-lg font-medium ${open && "hidden"}`}>
                  Daftar Karyawan
                </span>
              </a>
            </details>
            <li>
              <a
                href="/pasien"
                class="flex items-center p-2 text-white rounded-lg  hover:bg-green-500 text-lg"
              >
                <i class="fa-solid fa-stethoscope"></i>
                <span class="flex-1 ml-3 whitespace-nowrap">
                  Periksa Pasien
                </span>
              </a>
            </li>
            <li>
              <a
                href="/diag"
                class="flex items-center p-2 text-white rounded-lg  hover:bg-green-500 text-lg "
              >
                <i class="fa-solid fa-person-dots-from-line"></i>
                <span class="flex-1 ml-3 whitespace-nowrap">Diagnosa</span>
              </a>
            </li>
            <li>
              <a
                href="/pena"
                class="flex items-center p-2 text-white rounded-lg  hover:bg-green-500 text-lg"
              >
                <i class="fa-solid fa-hospital-user"></i>
                <span class="flex-1 ml-3 whitespace-nowrap">
                  Penanganan Pertama
                </span>
              </a>
            </li>
            <li>
              <a
                href="/tin"
                class="flex items-center p-2 text-white rounded-lg  hover:bg-green-500 text-lg"
              >
                <i class="fa-solid fa-location-crosshairs"></i>
                <span class="flex-1 ml-3 whitespace-nowrap">Tindakan</span>
              </a>
            </li>
            <li>
              <a
                href="/obat"
                class="flex items-center p-2 text-white rounded-lg  hover:bg-green-500 text-lg"
              >
                <i class="fa-sharp fa-solid fa-capsules"></i>
                <span class="flex-1 ml-3 whitespace-nowrap">
                  Daftar Obat P3K
                </span>
              </a>
            </li>
          </ul>
          <div className="text-center mt-8 text-base">{tanggal.format("LL")}</div>
          <div className="text-center text-xl">{currentTime}</div>
          <div
            onClick={logout}
            className="fixed bottom-0 flex items-center p-2 text-white rounded-lg  hover:bg-green-500 text-lg w-56"
          >
            <i className="fas fa-sign-out-alt"></i>
            <span class="flex-1 ml-3 whitespace-nowrap">Logout</span>
          </div>
          
        </div>
      </aside>
    </div>
  );
}
