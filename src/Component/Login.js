import axios from "axios";
import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import Swal from "sweetalert2";

export default function Login() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const [passwordType, setPasswordType] = useState("password");
  const [passwordIcon, setPasswordIcon] = useState("fa-solid fa-eye-slash");

  const togglePassword = () => {
    if (passwordType === "password") {
      setPasswordType("text");
      setPasswordIcon("fa-solid fa-eye");
      return;
    }
    setPasswordType("password");
    setPasswordIcon("fa-solid fa-eye-slash");
  };

  const history = useHistory();

  const login = async (e) => {
    e.preventDefault();

    try {
      const { data, status } = await axios.post(
        "http://localhost:2023/register/login",
        {
          email: email,
          password: password,
        }
      );
      // Jika respon 200/ ok
      if (status === 200) {
        Swal.fire({
          icon: "success",
          title: "Login Berhasil!!!",
          showConfirmButton: false,
          timer: 1500,
        });
        localStorage.setItem("Id", data.data.user.id);
        localStorage.setItem("username", data.data.user.username);
        localStorage.setItem("token", data.data.token);
        history.push("/home");
        setTimeout(() => {
          window.location.reload();
        }, 1500);
      }
    } catch (error) {
      Swal.fire({
        icon: "error",
        title: "Username atau password tidak valid!",
        showConfirmButton: false,
        timer: 1500,
      });
      console.log(error);
    }
  };
  return (
    <div className="mx-auto md:w-[40%] sm:px-6">
      <div className="mx-auto">
        <h1 className="text-center text-2xl font-bold text-green-900 hover:text-green-900 sm:text-3xl mt-10 shadow-lg shadow-blue-950">
          SISTEM APLIKASI UKS 
        </h1>
      <div className="ml-44 mt-5">
        <img
          width={150}
          src="https://avatars.githubusercontent.com/u/17557332?s=200&v=4"
          alt=""
        />
      </div>
        <form
          onSubmit={login}
          action=""
          className="mt-6 mb-0 space-y-4 rounded-lg p-4 shadow-lg border sm:p-6 lg:p-8"
        >
          <p className="text-center text-xl font-semibold">Login UKS</p>

          <div className="shadow-lg shadow-blue-950">
            <label className="sr-only ">Username / Email</label>

            <div className="relative">
              <input
                type="text"
                className="w-full rounded-lg border p-4 pr-12 text-sm shadow-sm"
                placeholder="Enter email or username"
                onChange={(e) => setEmail(e.target.value)}
                required
              />

              <span className="absolute inset-y-0 right-0 grid place-content-center px-4">
                <i className="fa-light fa-at text-lg font-semibold"></i>
              </span>
            </div>
          </div>

          <div className="shadow-lg shadow-blue-950">
            <label htmlFor="password" className="sr-only">
              Password
            </label>

            <div className="relative">
              <input
                value={password}
                type={passwordType}
                className="w-full rounded-lg border p-4 pr-12 text-sm shadow-sm"
                placeholder="Enter password"
                onChange={(e) => setPassword(e.target.value)}
              />

              <span
                onClick={togglePassword}
                className="absolute inset-y-0 right-0 grid place-content-center px-4"
              >
                <i class={passwordIcon}></i>
              </span>
            </div>
          </div>

          <button
            type="submit"
            className="block w-full rounded-lg bg-green-900 hover:bg-green-900 px-5 py-3 text-sm font-medium text-white"
          >
            Login
          </button>
        </form>
      </div>
    </div>
  );
}