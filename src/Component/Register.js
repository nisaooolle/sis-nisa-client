import axios from 'axios';
import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import Swal from 'sweetalert2';

export default function Register() {
  const [email, setEmail] = useState("");
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [passwordType, setPasswordType] = useState("password");
  const [passwordIcon ,setPasswordIcon] = useState("fa-solid fa-eye-slash")

  const togglePassword = () => {
    if (passwordType === "password") {
      setPasswordType("text");
      setPasswordIcon("fa-solid fa-eye")
      return;
    }
    setPasswordType("password");
    setPasswordIcon("fa-solid fa-eye-slash")
  };
  
  const history = useHistory();

  const register = async (e) => {
    e.preventDefault();
    try {
      await axios.post(
        "http://localhost:2023/register/register", {
        email: email,
        username: username,
        password: password,
      }
      )
        .then(() => {
          Swal.fire({
            icon: 'success',
            title: 'Berhasil Registrasi!! ',
            showConfirmButton: false,
            timer: 1500
          })
          setTimeout(() => {
            history.push('/')
            window.location.reload();
          }, 1500)
        })
    } catch (error) {
      alert("Terjadi Kesalahan " + error)
    }
  }
  return (
    <div className="mx-auto md:w-[40%] sm:px-6">
      <div className="mx-auto max-w-lg">
      <h1 className="text-center text-2xl font-bold text-green-900 hover:text-green-900 sm:text-3xl mt-10 shadow-lg shadow-blue-950">
          SISTEM APLIKASI UKS 
        </h1>
      <div className="ml-44 mt-5">
        <img
          width={150}
          src="http://103.133.27.106:3000/static/media/smpn1smg.174af4089a0f0045f9a4.png"
          alt=""
        />
      </div>
        <form
          onSubmit={register}
          action=""
          className=" mt-4 mb-0 space-y-2 rounded-lg px-4 shadow-lg border sm:px-6 sm:py-4 lg:px-8 lg:py-4"
        >
          <p className="text-center text-xl font-semibold">Register</p>
          <div>
            <label htmlFor="email" className="sr-only">Email</label>

            <div className="relative">
              <input
                type="email"
                className="w-full rounded-lg border px-4 md:py-2 py-1 pr-12 text-sm shadow-sm"
                placeholder="Enter email"
                onChange={(e) => setEmail(e.target.value)}
              />

              <span
                className="absolute inset-y-0 right-0 grid place-content-center px-4"
              >
                <i className="fa-light fa-at"></i>
              </span>
            </div>
          </div>
          <div>
            <label className="sr-only">Username</label>

            <div className="relative">
              <input
                type="username"
                className="w-full rounded-lg border px-4 md:py-2 py-1 pr-12 text-sm shadow-sm"
                placeholder="Enter username"
                onChange={(e) => setUsername(e.target.value)}
              />

              <span
                className="absolute inset-y-0 right-0 grid place-content-center px-4"
              >
               <i className="fa-solid fa-user"></i>
              </span>
            </div>
          </div>


          <div>
            <label htmlFor="password" className="sr-only">
              Password
            </label>

            <div className="relative">
              <input
                value={password}
                type={passwordType}
                className="w-full rounded-lg border px-4 md:py-2 py-1 pr-12 text-sm shadow-sm"
                placeholder="Enter password"
                onChange={(e) => setPassword(e.target.value)}
              />

              <span
                onClick={togglePassword}
                className="absolute inset-y-0 right-0 grid place-content-center px-4"
              >
                   <i class={passwordIcon}></i>
              </span>
            </div>
          </div>


          <button
            type="submit"
            className="block w-full rounded-lg bg-green-900 hover:bg-green-900 px-5 py-3 text-sm font-medium text-white"
          >
            Register
          </button>

          <p className="text-center text-sm">
            Sudah memiliki akun? <a className="link" href="/">Login</a>
          </p>
        </form>
      </div>
    </div>

  )
}
