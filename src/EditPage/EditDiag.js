import axios from 'axios';
import React from 'react'
import { useEffect } from 'react';
import { useState } from 'react';
import { useHistory, useParams } from 'react-router-dom';
import Swal from 'sweetalert2';

export default function EditDiag() {
    const [namaPenyakit, setDiagnosa] = useState();
  const param = useParams();
  const history = useHistory();
  const Put = async (e) => {
    e.preventDefault();

    try {
      await axios.put(`http://localhost:2023/diagnosa/` + param.id, {
        namaPenyakit: namaPenyakit,
      });
      Swal.fire({
        icon: "success",
        title: "Berhasil Mengedit",
        showConfirmButton: false,
        timer: 1500,
      });
      setTimeout(() => {
        history.push("/diag");
        window.location.reload();
      }, 1500);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    axios
      .get("http://localhost:2023/diagnosa/" + param.id)
      .then((response) => {
        const diagnosa = response.data.data;
        setDiagnosa(diagnosa.namaPenyakit);
      })
      .catch((error) => {
        alert("Terjadi Kesalahan " + error);
      });
  }, [param.id]);
  return (
    <div><div className="md:pl-11 pl-[4.5rem]">
    <form onSubmit={Put} className="space-y-3">
      <h3 className="md:py-7 py-4 md:text-2xl text-xl font-medium text-black dark:text-black">
        Edit Diagnosa
      </h3>
      <div className="md:flex gap-[3rem]">
        <div>
          <div>
            <label className="block md:mb-3 mb-2 md:text-base text-sm font-medium text-gray-900 dark:text-black">
              Nama Penyakit
            </label>
            <input
              placeholder="Nama Tindakan"
              onChange={(e) => setDiagnosa(e.target.value)}
              value={namaPenyakit}
              className="bg-gray-50 mb-4 border md:w-[30rem] w-[14rem] border-gray-300 text-gray-900 text-sm focus:ring-blue-500 focus:border-blue-500 block md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
              required
            />
          </div>
          <button
            type="submit"
            className="w-[5rem] md:ml-0 ml-[3rem] md:float-right rounded text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium text-sm px-4 md:py-2.5 py-1.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
          >
            Simpan
          </button>
          <a
            href="/tin"
            className="w-[5rem] md:mr-10 mr-0 rounded float-right text-white bg-red-700 hover:bg-red-800 focus:ring-4 focus:outline-none focus:ring-red-300 font-medium text-sm px-4 md:py-2.5 py-1.5 text-center dark:bg-red-600 dark:hover:bg-red-700 dark:focus:ring-red-800"
          >
            Batal
          </a>
        </div>
      </div>
    </form>
  </div></div>
  )
}
