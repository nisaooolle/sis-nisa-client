import axios from "axios";
import React from "react";
import { useEffect } from "react";
import { useState } from "react";
import { useHistory, useParams } from "react-router-dom";
import Swal from "sweetalert2";

export default function ProfileEdit() {
  const param = useParams();
  const [email, setEmail] = useState("");
  const [username, setUsername] = useState("");
  const [show, setShow] = useState(false);
  const [profile, setProfile] = useState({
    nama: "",
    email: "",
  });
  const history = useHistory();

  useEffect(() => {
    axios
      .get("http://localhost:2023/register/" + localStorage.getItem("Id"))
      .then((response) => {
        const profil = response.data.data;
        setEmail(profil.email);
        setUsername(profil.username)
      })
      .catch((error) => {
        alert("Terjadi Kesalahan " + error);
      });
  }, []);


  const putProfile = async (e) => {
    e.preventDefault();
    e.persist();

    try {
      await axios.put(
        `http://localhost:2026/sekolah/` + localStorage.getItem("userId"),
        {
          email: email,
          username: username
        }
      );
      setShow(false);
      Swal.fire({
        icon: "success",
        title: "Berhasil Mengedit",
        showConfirmButton: false,
        timer: 1500,
      });
      setTimeout(() => {
        history.push("/profile");
        window.location.reload();
      }, 1500);
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <div>
      <div className="shadow-2xl mt-5 md:w-[35%] md:ml-[30%] px-2 py-6 lg:px-8 bg-blue-500/10">
        <div className="flex md:gap-x-[18.6rem] gap-x-[11.7rem]">
          <h3 className="mb-4 text-xl font-medium text-gray-900 dark:text-black">
            Edit Profil
          </h3>
          <button>
            <a href="/profile">
              <i className="fas fa-times bg-gray-200 p-2 rounded-lg"></i>
            </a>
          </button>
        </div>
        <form onSubmit={putProfile} className="space-y-3">
          <div>
            <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-black">
              UserName
            </label>
            <input
              placeholder="UserName"
              value={username}
              onChange={(e) => setUsername(e.target.value)}
              className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
              required
            />
          </div>
          <div>
            <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-black">
              Email
            </label>
            <input
              placeholder=" Email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
              className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
              required
            />
          </div>
          <button
            type="submit"
            className="w-full text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
          >
            Simpan
          </button>
        </form>
      </div>
    </div>
  );
}
