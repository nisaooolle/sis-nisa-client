import axios from "axios";
import React from "react";
import { useEffect } from "react";
import { useState } from "react";
import { useHistory, useParams } from "react-router-dom";
import Swal from "sweetalert2";

export default function EditSiswa() {
  const [username, setSiswa] = useState();
  const [tempat, setTmptlhr] = useState();
  const [tanggalLahir, setTgllhr] = useState();
  const [alamat, setAlamat] = useState();
  const [kelas, setKelas] = useState();
  const param = useParams();
  const history = useHistory();
  const Put = async (e) => {
    e.preventDefault();

    try {
      await axios.put(`http://localhost:2023/data/${param.id}_siswa`, {
        username: username,
        kelas: kelas,
        tempat: tempat,
        tanggalLahir: tanggalLahir,
        alamat: alamat,
      });
      Swal.fire({
        icon: "success",
        title: "Berhasil Mengedit",
        showConfirmButton: false,
        timer: 1500,
      });
      setTimeout(() => {
        history.push("/siswa");
        window.location.reload();
      }, 1500);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    axios
      .get("http://localhost:2023/data/" + param.id)
      .then((response) => {
        const diagnosa = response.data.data;
        setSiswa(diagnosa.username);
        setKelas(diagnosa.kelas);
        setTmptlhr(diagnosa.tempat);
        setTgllhr(diagnosa.tanggalLahir);
        setAlamat(diagnosa.alamat);
      })
      .catch((error) => {
        alert("Terjadi Kesalahan " + error);
      });
  }, [param.id]);
  return (
    <div>
      <div className="md:pl-11 pl-[4.5rem]">
        <form onSubmit={Put} className="space-y-3">
          <h3 className="md:py-7 py-4 md:text-2xl text-xl font-medium text-black dark:text-black">
            Edit Daftar Siswa
          </h3>
          <div className="md:flex gap-[3rem]">
            <div>
              <div>
                <label className="block md:mb-3 mb-2 md:text-base text-sm font-medium text-gray-900 dark:text-black">
                  Nama Siswa
                </label>
                <input
                  placeholder="Nama Siswa"
                  onChange={(e) => setSiswa(e.target.value)}
                  value={username}
                  className="bg-gray-50 mb-4 border md:w-[30rem] w-[14rem] border-gray-300 text-gray-900 text-sm focus:ring-blue-500 focus:border-blue-500 block md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
                  required
                />
              </div>
              <div>
                <label className="block mb-2 text-sm font-medium text-black dark:text-black">
                  Kelas
                </label>
                <select onChange={(e) => setKelas(e.target.value)}
                    className="bg-gray-50 border border-gray-300 text-black text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-500 dark:text-black"
                    aria-label="kelas"
                  >
                    <option class="hidden">Pilih Kelas</option>
                    <option value = "VII A">VII A</option>
                    <option value = "VII B"> VII B</option>
                    <option value = "VII C">VII C</option>
                    <option value = "VII D">VII D</option>
                    <option value = "VII E">VII E</option>
                    <option value = "VII F">VII F</option>
                    <option value = "VII G">VII G</option>
                    <option value = "VII H">VII H</option>
                    <option value = "VII I">VII I</option>
                    <option value = "VIII A">VIII A</option>
                    <option value = "VIII B">VIII B</option>
                    <option value = "VIII C">VIII C</option>
                    <option value = "VIII D">VIII D</option>
                    <option value = "VIII E">VIII E</option>
                    <option value = "VIII F">VIII F</option>
                    <option value = "VIII G">VIII G</option>
                    <option value = "VIII H">VIII H</option>
                    <option value = "VIII I">VIII I</option>
                    <option value = "IX A">IX A</option>
                    <option value = "IX B">IX B</option>
                    <option value = "IX C">IX C</option>
                    <option value = "IX D">IX D</option>
                    <option value = "IX E">IX E</option>
                    <option value = "IX F">IX F</option>
                    <option value = "IX G">IX G</option>
                    <option value = "IX H">IX H</option>
                    <option value = "IX I">IX I</option>
                  </select>
              </div>
              <div>
                <label className="block md:mb-3 mb-2 md:text-base text-sm font-medium text-gray-900 dark:text-black">
                  Tempat Lahir
                </label>
                <input
                  placeholder="Tempat Lahir"
                  onChange={(e) => setTmptlhr(e.target.value)}
                  value={tempat}
                  className="bg-gray-50 mb-4 border md:w-[30rem] w-[14rem] border-gray-300 text-gray-900 text-sm focus:ring-blue-500 focus:border-blue-500 block md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
                  required
                />
              </div>
              <div>
                <label className="block md:mb-3 mb-2 md:text-base text-sm font-medium text-gray-900 dark:text-black">
                  Tanggal Lahir
                </label>
                <input
                  //   placeholder="Nama Tindakan"
                  type="date"
                  onChange={(e) => setTgllhr(e.target.value)}
                  value={tanggalLahir}
                  className="bg-gray-50 mb-4 border md:w-[30rem] w-[14rem] border-gray-300 text-gray-900 text-sm focus:ring-blue-500 focus:border-blue-500 block md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
                  required
                />
              </div>
              <div>
                <label className="block md:mb-3 mb-2 md:text-base text-sm font-medium text-gray-900 dark:text-black">
                  Alamat
                </label>
                <input
                  placeholder="alamat"
                  onChange={(e) => setAlamat(e.target.value)}
                  value={alamat}
                  className="bg-gray-50 mb-4 border md:w-[30rem] w-[14rem] border-gray-300 text-gray-900 text-sm focus:ring-blue-500 focus:border-blue-500 block md:p-1.5 p-1 dark:bg-white dark:border-gray-500 dark:placeholder-gray-400 dark:text-black"
                  required
                />
              </div>
              <button
                type="submit"
                className="w-[5rem] md:ml-0 ml-[3rem] md:float-right rounded text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium text-sm px-4 md:py-2.5 py-1.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
              >
                Simpan
              </button>
              <a
                href="/tin"
                className="w-[5rem] md:mr-10 mr-0 rounded float-right text-white bg-red-700 hover:bg-red-800 focus:ring-4 focus:outline-none focus:ring-red-300 font-medium text-sm px-4 md:py-2.5 py-1.5 text-center dark:bg-red-600 dark:hover:bg-red-700 dark:focus:ring-red-800"
              >
                Batal
              </a>
            </div>
          </div>
        </form>
      </div>
    </div>
  );
}
