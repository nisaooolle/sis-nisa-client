import { BrowserRouter, Route, Switch } from "react-router-dom";
import Home from "./Component/Home";
// import SideBar from "./Component/SideBar";
import Dash from "./Component/Dash";
import Register from "./Component/Register";
import Login from "./Component/Login";
import Penanganan from "./Page/Penanganan";
import Tindakan from "./Page/Tindakan";
import Diagnosa from "./Page/Diagnosa";
import DaftarObat from "./Page/DaftarObat";
import DaFtaGuru from "./Page/DaftarGuru";
import DaftarSiswa from "./Page/DaftarSiswa";
import DaftarKaryawan from "./Page/DaftarKaryawan";
import EditTindakan from "./EditPage/EditTindakan";
import EditPenanganan from "./EditPage/EditPenanganan";
import EditDiag from "./EditPage/EditDiag";
import EditDaftarObat from "./EditPage/EditDaftarObat";
import EditGuru from "./EditPage/EditGuru";
import EditKaryawan from "./EditPage/EditKaryawan";
import EditSiswa from "./EditPage/EditSiswa";
 import Profile from "./Page/Profile";
import ProfileEdit from "./EditPage/ProfileEdit";
import Pasien from "./Page/Pasien";
import TanganiPasien from "./Page/TanganiPasien";

function App() {
  return (
    <div className="App">
      <BrowserRouter>
      <Switch>
    <Route path="/" component={Login} exact />
    <Route path="/home" component={Home} exact />
    <Route path="/dash" component={Dash} exact />
    <Route path="/reg" component={Register} exact />
    <Route path="/pena" component={Penanganan} exact />
    <Route path="/tin" component={Tindakan} exact />
    <Route path="/diag" component={Diagnosa} exact />
    <Route path="/obat" component={DaftarObat} exact />
    <Route path="/guru" component={DaFtaGuru} exact />
    <Route path="/siswa" component={DaftarSiswa} exact />
    <Route path="/karyawan" component={DaftarKaryawan} exact />
    <Route path="/proff" component={Profile} exact />
    <Route path="/pasien" component={Pasien} exact />
    <Route path="/editTin/:id" component={EditTindakan} exact />
    <Route path="/editPena/:id" component={EditPenanganan} exact />
    <Route path="/editDiag/:id" component={EditDiag} exact />
    <Route path="/editObat/:id" component={EditDaftarObat} exact />
    <Route path="/editGuru/:id" component={EditGuru} exact />
    <Route path="/editKaryawan/:id" component={EditKaryawan} exact />
    <Route path="/editSiswa/:id" component={EditSiswa} exact />
    <Route path="/editprofile/:id" component={ProfileEdit} exact />
    <Route path="/tanganiPasien/:id" component={TanganiPasien} exact />
      </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
